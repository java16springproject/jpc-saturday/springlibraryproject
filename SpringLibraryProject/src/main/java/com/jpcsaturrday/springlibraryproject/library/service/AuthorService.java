package com.jpcsaturrday.springlibraryproject.library.service;

import com.jpcsaturrday.springlibraryproject.library.dto.AuthorDTO;
import com.jpcsaturrday.springlibraryproject.library.mapper.AuthorMapper;
import com.jpcsaturrday.springlibraryproject.library.model.Author;
import com.jpcsaturrday.springlibraryproject.library.model.Book;
import com.jpcsaturrday.springlibraryproject.library.repository.AuthorRepository;
import com.jpcsaturrday.springlibraryproject.library.repository.BookRepository;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

@Service
public class AuthorService
        extends GenericService<Author, AuthorDTO> {

    private final BookRepository bookRepository;


    public AuthorService(AuthorRepository repository,
                         AuthorMapper authorMapper,
                         BookRepository bookRepository) {
        super(repository, authorMapper);
        this.bookRepository = bookRepository;
    }

    public AuthorDTO addBook(Long bookId,
                             Long authorId) {
        Book book = bookRepository.findById(bookId).orElseThrow(() -> new NotFoundException("Книга не найдена!"));
        AuthorDTO author = getOne(authorId);
        author.getBookIds().add(book.getId());
        update(author);
        return author;
    }
}
