package com.jpcsaturrday.springlibraryproject.library.controllers.rest;



import com.jpcsaturrday.springlibraryproject.library.dto.BookRentInfoDTO;
import com.jpcsaturrday.springlibraryproject.library.model.BookRentInfo;
import com.jpcsaturrday.springlibraryproject.library.service.BookRentInfoService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rent/info")
@Tag(name = "Аренда книг",
     description = "Контроллер для работы с арендой/выдачей книг пользователям библиотеки")
public class RentBookController
      extends GenericController<BookRentInfo, BookRentInfoDTO> {
    
    public RentBookController(BookRentInfoService bookRentInfoService) {
        super(bookRentInfoService);
    }
}
