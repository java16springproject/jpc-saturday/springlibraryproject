package com.jpcsaturrday.springlibraryproject.library.repository;

import com.jpcsaturrday.springlibraryproject.library.model.Book;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository
        extends GenericRepository<Book> {
}
