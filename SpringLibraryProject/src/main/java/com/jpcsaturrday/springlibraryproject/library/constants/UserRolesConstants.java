package com.jpcsaturrday.springlibraryproject.library.constants;

public interface UserRolesConstants {
    String ADMIN = "ADMIN";
    String USER = "USER";
    String LIBRARIAN = "LIBRARIAN";
}
