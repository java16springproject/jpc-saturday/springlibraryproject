package com.jpcsaturrday.springlibraryproject.library.repository;

import com.jpcsaturrday.springlibraryproject.library.model.Author;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorRepository
        extends GenericRepository<Author> {
}
