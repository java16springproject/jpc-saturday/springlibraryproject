package com.jpcsaturrday.springlibraryproject.library.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;
import java.util.List;

@ToString
@NoArgsConstructor
@Getter
@Setter
public class AuthorDTO extends GenericDTO {
    private String authorFIO;
    private LocalDate birthDate;
    private String description;
    private List<Long> bookIds;

//    public AuthorDTO(Author author) {
//        birthDate = author.getBirthDate();
//        authorFIO = author.getAuthorFIO();
//        description = author.getDescription();
//        id = author.getId();
//        createdWhen = author.getCreatedWhen();
//        List<Book> books = author.getBooks();
//        List<Long> bookIds = new ArrayList<>();
//        books.forEach(book -> bookIds.add(book.getId()));
//        this.bookIds = bookIds;
//    }
//
//    public static List<AuthorDTO> getAuthorsDTO(List<Author> authors) {
//        List<AuthorDTO> authorDTOS = new ArrayList<>();
//        for (Author author : authors) {
//            authorDTOS.add(new AuthorDTO(author));
//        }
//        return authorDTOS;
//    }
}
