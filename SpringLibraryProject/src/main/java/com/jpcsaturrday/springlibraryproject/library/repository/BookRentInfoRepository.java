package com.jpcsaturrday.springlibraryproject.library.repository;

import com.jpcsaturrday.springlibraryproject.library.model.BookRentInfo;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRentInfoRepository
        extends GenericRepository<BookRentInfo> {
}
