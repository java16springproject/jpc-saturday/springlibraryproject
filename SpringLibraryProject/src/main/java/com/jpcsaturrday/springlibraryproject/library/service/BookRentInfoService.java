package com.jpcsaturrday.springlibraryproject.library.service;



import com.jpcsaturrday.springlibraryproject.library.dto.BookRentInfoDTO;
import com.jpcsaturrday.springlibraryproject.library.mapper.BookRentInfoMapper;
import com.jpcsaturrday.springlibraryproject.library.model.BookRentInfo;
import com.jpcsaturrday.springlibraryproject.library.repository.BookRentInfoRepository;
import org.springframework.stereotype.Service;

@Service
public class BookRentInfoService
      extends GenericService<BookRentInfo, BookRentInfoDTO> {
    protected BookRentInfoService(BookRentInfoRepository bookRentInfoRepository,
                                  BookRentInfoMapper bookRentInfoMapper) {
        super(bookRentInfoRepository, bookRentInfoMapper);
    }
}
